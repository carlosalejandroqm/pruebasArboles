package pruebas;

/**
 *
 * @author carlos
 */
import java.util.*;

import util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);

        ArbolBinario<Integer> ab = new ArbolBinario<>();
        ArbolBinarioBusqueda<Integer> abb = new ArbolBinarioBusqueda<>();
        ArbolBinarioBusqueda<Integer> abb2 = new ArbolBinarioBusqueda<>();

        abb.insertar(9);
        abb.insertar(7);
        abb.insertar(11);
        abb.insertar(0);
        abb.insertar(8);
        abb.insertar(10);
        abb.insertar(12);
        abb.insertar(13);
        abb.insertar(14);
        
        abb2.insertar(9);
        abb2.insertar(7);
        abb2.insertar(11);
        abb2.insertar(0);
        abb2.insertar(8);
        abb2.insertar(10);
        abb2.insertar(12);
        abb2.insertar(13);
        abb2.insertar(15);
        
        System.out.println(abb.mismos(abb2));
        
        
        /*
      
        abb.insertar(15);
        abb.insertar(3);
        abb.insertar(4);
         */
        //System.out.println(abb.camino(10).toString());
        
        //System.out.println(abb.porNiveles().toString());
       // System.out.println(abb.caminosInfos(13, 13).toString());
        
        //System.out.println(abb.camino(12).toString()); // funciona bien 

        // duda en camino 
        System.out.println(abb.camino(10).toString());
        // prueba preorden iterativo ( funciona ) 
        // System.out.println(abb.inorden2());
        // imprime la rama mas larga ( funciona ) 
        //System.out.println(abb.ramaLarga().toString() + "\n");
        // imprime el arbol en posorden ( no funciona ) 
        //System.out.println(abb.postOrden2().toString());
        /*
        abb.imprime();
        System.out.println("imprimiendo el arbol por niveles: ");
        System.out.println(abb.porNiveles().toString() + "\n");
        
        System.out.println("imprimiendo inorder iterativo: ");
        abb.inorderAlejo();
        
        System.out.println("imprimiendo posorden en iterativo: ");
        System.out.println(abb.postOrden2());
        
        System.out.println("imprimiendo el postorder en recursivo ( para comparar) ");
        abb.postorderAlejo();
         */
       // ArbolBinarioBusqueda<Integer> abb1 = new ArbolBinarioBusqueda<>();
        //ArbolBinarioBusqueda<Integer> abb2 = new ArbolBinarioBusqueda<>();

        // System.out.println(abb1.getArea(abb2));
    }

}


/*prueba preorden , inorder , postorder y buscar  alejo ( funciona bien ) 
        abb.preordenALejo();
        abb.inorderAlejo();
        abb.postorderAlejo();
        System.out.println(abb.buscarAlejo(9));
 */
